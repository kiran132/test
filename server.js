const express = require("express")

const app = express()

app.get("/",(request,response)=>{

    response.send("response from the container")
})

app.listen(3000,"0.0.0.0",()=>{
    console.log(" The server started on port no 3000")
})